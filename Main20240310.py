import threading
import time
from uuid import uuid4

"""
This is your coding interview problem for today.

This problem was asked by Apple.

Implement a job scheduler which takes in a function f and an integer n, and calls f after n milliseconds.

We will be sending the solution tomorrow, along with tomorrow's question. As always, feel free to shoot us an email if there's anything we can help with.

Have a great day!
"""
class JobScheduler:

    def __init__(self, method: callable, millis: int):
        
        def execute_each_interval():
            while self.keep_executing:
                time.sleep(millis / 1000.0)
                method()

        self.keep_executing = True

        self.__job_thread = threading.Thread(
            target=execute_each_interval,
            name=JobScheduler.get_thread_name()
        )

    def start(self):
        self.keep_executing = True
        self.__job_thread.start()

    def stop(self):
        self.keep_executing = False
    
    @staticmethod
    def get_thread_name() -> str:
        return  f'scheduler-thread-{uuid4().hex}'

    @staticmethod
    def get_join_timeout() -> float:
        # Waits execution finishes for 5 seconds
        return 5.0


# METHOD 1
def print_hello_world():
    print(f'Hello World from {threading.current_thread().name}!')

# Method 2
def print_another_test():
    print(f'Another test from {threading.current_thread().name}!')


if __name__ == '__main__':
    scheduler_one = JobScheduler(print_hello_world, 1000)
    scheduler_two = JobScheduler(print_another_test, 10000)


    #Start all threads
    scheduler_one.start()
    scheduler_two.start()

    #Stop first thread
    time.sleep(30)
    print('Stopping first thread!')
    scheduler_one.stop()

    #Wait for 30 seconds and finish the other thread
    time.sleep(60)
    print('Stopping second thread!')
    scheduler_two.stop()
