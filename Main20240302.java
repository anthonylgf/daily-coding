import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main20240302 {

    /**
     * This is your coding interview problem for today.
     *
     * This problem was asked by Uber.
     *
     * Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in the original array except the one at i.
     *
     * For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].
     * */
    public static void main(String[] args) throws IOException {
        var reader = new BufferedReader(new InputStreamReader(System.in));

        int[] numbers = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();

        //It is neutral value for multiply operations
        int[] result = Arrays.copyOf(numbers, numbers.length);

        for (int i = 0; i < numbers.length; i++) {
            // Replace value for a neutral one
            int temp = numbers[i];
            numbers[i] = 1;

            result[i] = Arrays.stream(numbers).reduce(1, Math::multiplyExact);

            // Put value back after ops
            numbers[i] = temp;
        }

        System.out.println(Arrays.toString(result));
    }

}
