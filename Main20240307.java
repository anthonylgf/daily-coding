import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main20240307 {

    /**
     * This problem was asked by Facebook.
     *
     * Given the mapping a = 1, b = 2, ... z = 26, and an encoded message, count the number of ways it can be decoded.
     *
     * For example, the message '111' would give 3, since it could be decoded as 'aaa', 'ka', and 'ak'.
     *
     * You can assume that the messages are decodable. For example, '001' is not allowed.
     *
     * We will be sending the solution tomorrow, along with tomorrow's question. As always, feel free to shoot us an email if there's anything we can help with.
     *
     * Have a great day!
     */
    public static void main(String[] args) throws IOException {
        var reader = new BufferedReader(new InputStreamReader(System.in));

        String encoded = reader.readLine();

        System.out.println(numRepetion(encoded));
    }

    private static int numRepetion(String encoded) {
        if (encoded.length() < 2) {
            return 1;
        }

        char num = encoded.charAt(0);

        if (num > '2') {
            return numRepetion(encoded.substring(1));
        }

        char nextNum = encoded.charAt(1);

        if (num == '2' && nextNum > '6') {
            return numRepetion(encoded.substring(1));
        }

        return numRepetion(encoded.substring(1)) + numRepetion(encoded.substring(2));
    }
}
