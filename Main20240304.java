import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main20240304 {

    /**
     * This is your coding interview problem for today.
     *
     * This problem was asked by Stripe.
     *
     * Given an array of integers, find the first missing positive integer in linear time and constant space. In other words, find the lowest positive integer that does not exist in the array. The array can contain duplicates and negative numbers as well.
     *
     * For example, the input [3, 4, -1, 1] should give 2. The input [1, 2, 0] should give 3.
     *
     * You can modify the input array in-place.
     *
     * We will be sending the solution tomorrow, along with tomorrow's question. As always, feel free to shoot us an email if there's anything we can help with.
     *
     * Have a great day!
     */
    public static void main(String[] args) throws IOException {
        var reader = new BufferedReader(new InputStreamReader(System.in));

        int[] numbers = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();


        System.out.println(getMinPositive(numbers));
    }

    private static int getMinPositive(int[] numbers) {
        int min = 1;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] <= 0) {
                numbers[i] = Integer.MAX_VALUE;
            }

            if (i > 0 && (numbers[i] > numbers[i - 1])) {
                swapAtIndex(numbers, i);
            }

            if(numbers[i] <= min) {
                min = incrementMin(numbers, i, min);
            }
        }

        return min;
    }

    private static int incrementMin(int[] number, int index, int currentMin) {

        int result = currentMin;

        while(index >= 0) {
            if(result < number[index]) {
                return result;
            }

            result = number[index] + 1;

            index--;
        }

        return result;
    }

    private static void swapAtIndex(int[] numbers, int index) {

        while (index > 0 && numbers[index] > numbers[index - 1]) {
            var temp = numbers[index - 1];
            numbers[index - 1] = numbers[index];
            numbers[index] = temp;

            index--;
        }

    }

}
