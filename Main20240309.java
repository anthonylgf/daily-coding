import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * This is your coding interview problem for today.
 *
 * This problem was asked by Airbnb.
 *
 * Given a list of integers, write a function that returns the largest sum of non-adjacent numbers. Numbers can be 0 or negative.
 *
 * For example, [2, 4, 6, 2, 5] should return 13, since we pick 2, 6, and 5. [5, 1, 1, 5] should return 10, since we pick 5 and 5.
 *
 * Follow-up: Can you do this in O(N) time and constant space?
 *
 * We will be sending the solution tomorrow, along with tomorrow's question. As always, feel free to shoot us an email if there's anything we can help with.
 *
 * Have a great day!
 */
public class Main20240309 {

    private static int SUM_MAX = Integer.MIN_VALUE;

    public static void main(String[] args) throws IOException {
        var reader = new BufferedReader(new InputStreamReader(System.in));

        int[] numbers = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .toArray();


        getArrayMax(numbers);

        System.out.println(SUM_MAX);
    }

    private static void getArrayMax(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] <= 0 && SUM_MAX > numbers[i]) {
                continue;
            }

            getArrayMax(numbers, i + 2, numbers[i]);
        }
    }

    private static void getArrayMax(int[] numbers, int startIdx, int currSum) {
        SUM_MAX = Math.max(currSum, SUM_MAX);

        if (numbers.length - startIdx <= 0) {
            return;
        }

        if (numbers.length - startIdx <= 2) {
            SUM_MAX = Math.max(SUM_MAX, currSum + numbers[startIdx]);
            return;
        }

        for (int i = startIdx; i < numbers.length; i++) {
            if (numbers[i] <= 0) {
                continue;
            }

            getArrayMax(numbers, i + 2, currSum + numbers[i]);
        }
    }

}
