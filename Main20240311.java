import java.util.ArrayList;
import java.util.List;

/**
 * Problem from LEETCODE
 * 
 * 
  * Given the root of a binary tree, imagine yourself standing on the right side of it, return the values of the nodes you can see ordered from top to bottom.
  * 
  *  
  * 
  * Example 1:
  * 
  * 
  * Input: root = [1,2,3,null,5,null,4]
  * Output: [1,3,4]
  * Example 2:
  * 
  * Input: root = [1,null,3]
  * Output: [1,3]
  * Example 3:
  * 
  * Input: root = []
  * Output: []
  *  
  * 
  * Constraints:
  * 
  * The number of nodes in the tree is in the range [0, 100].
  * -100 <= Node.val <= 100
 */
public class Main20240311 {
    public List<Integer> rightSideView(TreeNode root) {

        if(root == null) {
            return List.of();
        }
        
        var result = new ArrayList<Integer>();

        result.add(root.val);

        walkThrougTree(root.right, 1, result);
        walkThrougTree(root.left, 1, result);

        return result;
    }

    private void walkThrougTree(TreeNode node, int depth, List<Integer> result) {
        if(node == null) {
            return;
        }

        if(result.size() == depth) {
            result.add(node.val);
        }

        walkThrougTree(node.right, depth + 1, result);
        walkThrougTree(node.left, depth + 1, result);
    }
}


class TreeNode {
         int val;
         TreeNode left;
         TreeNode right;
         TreeNode() {}
         TreeNode(int val) { this.val = val; }
         TreeNode(int val, TreeNode left, TreeNode right) {
             this.val = val;
             this.left = left;
             this.right = right;
         }
     }