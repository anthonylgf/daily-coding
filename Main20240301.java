import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

import static java.lang.Integer.parseInt;

public class Main20240301 {

    /**
     * PROBLEM Description[EASY]:
     *
     * Good morning! Here's your coding interview problem for today.
     *
     * This problem was recently asked by Google.
     *
     * Given a list of numbers and a number k, return whether any two numbers from the list add up to k.
     *
     * For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.
     *
     * Bonus: Can you do this in one pass?
     * */
    public static void main(String[] args) throws IOException {

        var reader = new BufferedReader(new InputStreamReader(System.in));

        int k = parseInt(reader.readLine());

        int[] numbers = Arrays.stream(reader.readLine().split(" "))
                .mapToInt(Integer::parseInt)
                .filter(i -> i < k)
                .sorted()
                .toArray();

        var result = Arrays.stream(numbers)
                .anyMatch(i -> Arrays.binarySearch(numbers, (k - i)) >= 0);

        System.out.println(result);
    }

}
