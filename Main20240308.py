from typing import Tuple

"""
This problem was asked by Google.

A unival tree (which stands for "universal value") is a tree where all nodes under it have the same value.

Given the root to a binary tree, count the number of unival subtrees.

For example, the following tree has 5 unival subtrees:

   0
  / \
 1   0
    / \
   1   0
  / \
 1   1
"""
class Node:

    def __init__(self, value: int) -> None:
        self.value = value
        self.left: Node = None
        self.right: Node = None

    def count_unival(self) -> Tuple[int, bool]:

        if not self.left and not self.right:
            return 1, True
        
        is_root_unival = True
        count_sub_unival = 0

        if self.left:
            is_root_unival = is_root_unival and (self.left.value == self.value)

            is_sub_unival, sub_unival = self.left.count_unival()
            count_sub_unival += sub_unival
            is_root_unival = is_root_unival and is_sub_unival

        if self.right:
            is_sub_unival, sub_unival = self.right.count_unival()
            count_sub_unival += sub_unival
            is_root_unival = is_root_unival and is_sub_unival

        if is_root_unival:
            count_sub_unival += 1
        
        return count_sub_unival, is_root_unival
    

if __name__ == '__main__':
    root = Node(0)

    rn1 = Node(1)
    ln1 = Node(0)

    root.right = rn1
    root.left = ln1

    rn2 = Node(1)
    ln2 = Node(0)

    ln1.right = rn2
    ln1.left = ln2

    rn3 = Node(1)
    ln3 = Node(1)

    rn2.right = rn3
    rn2.left = ln3


    print(root.count_unival([0]))