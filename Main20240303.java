import java.io.IOException;
import java.io.Serializable;
import java.util.Objects;

public class Main20240303 {

    /**
     * Here's a solution to yesterday's problem.
     *
     * This is your coding interview problem for today.
     *
     * This problem was asked by Google.
     *
     * Given the root to a binary tree, implement serialize(root), which serializes the tree into a string, and deserialize(s), which deserializes the string back into the tree.
     *
     * For example, given the following Node class
     *
     * class Node:
     *     def __init__(self, val, left=None, right=None):
     *         self.val = val
     *         self.left = left
     *         self.right = right
     * The following test should pass:
     *
     * node = Node('root', Node('left', Node('left.left')), Node('right'))
     * assert deserialize(serialize(node)).left.left.val == 'left.left'
     * We will be sending the solution tomorrow, along with tomorrow's question. As always, feel free to shoot us an email if there's anything we can help with.
     *
     * Have a great day!
     * */
    public static void main(String[] args) throws IOException {
        // Execute tests
        var node1 = new Node(
                "root",
                new Node(
                        "left",
                        new Node("left.left")
                ),
                new Node("right")
        );

        assert "left.left".equals(Node.deserialize(Node.serialize(node1)).left.left.val);

        // Execute tests
        var node2 = new Node(
                "root",
                new Node(
                        "left",
                        new Node(
                                "left.left",
                                new Node(
                                        "left.left.left",
                                        new Node("left.left.left.left"),
                                        new Node("left.left.left.right")
                                ),
                                new Node("left.left.right")
                        ),
                        new Node("left.right")
                ),
                new Node(
                        "right",
                        new Node(
                                "right.left",
                                new Node(
                                        "right.left.left",
                                        null,
                                        new Node("right.left.left.right")
                                ),
                                new Node(
                                        "right.left.right",
                                        new Node(
                                                "right.left.right.left",
                                                null,
                                                new Node("right.left.right.left")
                                        ),
                                        new Node("right.left.right.left")
                                )
                        ),
                        new Node(
                                "right.right",
                                null,
                                new Node(
                                        "right.right.right",
                                        new Node(
                                                "right.right.right.left",
                                                null,
                                                new Node("right.right.right.left.right")
                                        ),
                                        new Node(
                                                "right.right.right.right",
                                                new Node("right.right.right.right.left")
                                        )
                                )
                        )
                )
        );

        var baseValue = node2.right.right.right.right.left.val;
        var result = Node.deserialize(Node.serialize(node2)).right.right.right.right.left.val;

        assert baseValue.equals(result);
    }

}

class Node implements Serializable {

    public String val;

    public Node left;

    public Node right;

    public Node(String val, Node left, Node right) {
        Objects.requireNonNull(val, "Value can not be null");

        this.val = val;
        this.left = left;
        this.right = right;
    }

    public Node(String val, Node left) {
        this(val, left, null);
    }

    public Node(String val) {
        this(val, null, null);
    }

    public static String serialize(Node node) {
        return node.toString();
    }

    public static Node deserialize(String treeRepr) {
        return deserialize(treeRepr, 0);
    }

    private static Node deserialize(String treeRepr, int level) {

        int firstCommaIdx = treeRepr.indexOf(',');

        // Get value that arte substr between 'Node{val=' and first comma
        var val = treeRepr.substring(9, firstCommaIdx);

        Node left = null;

        int rigthIndexToSearch;

        //Check if exists a left node and process it
        if ("Node".equals(treeRepr.substring(firstCommaIdx + 6, firstCommaIdx + 10))) {
            var endNode = treeRepr.indexOf("level=" + (level + 1)) + 8;

            left = deserialize(treeRepr.substring(firstCommaIdx + 6, endNode), level + 1);

            rigthIndexToSearch = endNode + 7;
        } else {
            // Start to search after 'Node{val=*,left=null,right=' clause
            rigthIndexToSearch = firstCommaIdx + 17;
        }

        Node right = null;

        //Check if exists a right node and process it
        if ("Node".equals(treeRepr.substring(rigthIndexToSearch, rigthIndexToSearch + 4))) {
            var endNode = treeRepr.indexOf("level=" + (level + 1), rigthIndexToSearch) + 8;

            right = deserialize(treeRepr.substring(rigthIndexToSearch, endNode), level + 1);
        }


        return new Node(val, left, right);
    }

    @Override
    public String toString() {
        return toString(0);
    }

    private String toString(int level) {
        return String.format("Node{val=%s,left=%s,right=%s,level=%d}",
                this.val,
                this.left != null ? this.left.toString(level + 1) : null,
                this.right != null ? this.right.toString(level + 1) : null,
                level
        );
    }
}
