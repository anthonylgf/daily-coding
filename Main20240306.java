import java.util.HashMap;
import java.util.Map;

import static java.lang.System.identityHashCode;

/**
 * This problem was asked by Google.
 *
 * An XOR linked list is a more memory efficient doubly linked list. Instead of each node holding next and prev fields, it holds a field named both, which is an XOR of the next node and the previous node. Implement an XOR linked list; it has an add(element) which adds the element to the end, and a get(index) which returns the node at index.
 *
 * If using a language that has no pointers (such as Python), you can assume you have access to get_pointer and dereference_pointer functions that converts between nodes and memory addresses.
 */
public class Main20240306 {

    private static final int NULL = 0;

    private static final Map<Integer, Node> referenceToObj = new HashMap<>();

    public static void main(String[] args) {
        test1();
        test2();
    }

    private static void test1() {
        var list = new CustomList();

        Node first = null;

        Node tenth = null;

        Node thirtyOneth = null;

        int len = 50;

        for (int i = 0; i < len; i++) {
            var node = new Node();

            list.add(node);

            if (i == 0) {
                first = node;
            } else if (i == 9) {
                tenth = node;
            } else if (i == 30) {
                thirtyOneth = node;
            }
        }


        checkCondition(list.get(0) == first);
        checkCondition(list.get(9) == tenth);
        checkCondition(list.get(30) == thirtyOneth);
        checkCondition(list.get(len) == null);
    }

    private static void checkCondition(boolean condition) {
        if (!condition) {
            throw new RuntimeException("Error on condition");
        }
    }

    private static void test2() {
        var list = new CustomList();

        if (list.get(0) != null) {
            throw new RuntimeException("Error on list operation!");
        }

        var node = new Node();

        list.add(node);

        if (list.get(0) != node) {
            throw new RuntimeException("Error on list operation");
        }
    }


    private static class Node {
        int address;

        int both;

        Node() {
            this.address = identityHashCode(this);
            this.both = 0;

            referenceToObj.put(this.address, this);
        }

        void addPrev(Node prev) {
            this.both = prev.address;
        }

        void addNext(Node next) {
            this.both ^= next.address;
        }
    }


    private static class CustomList {

        Node root;

        CustomList() {
            this.root = null;
        }

        void add(Node node) {
            if (identityHashCode(this.root) == NULL) {
                this.root = node;
                return;
            }

            add(node, this.root, NULL);
        }

        private void add(Node node, Node prev, int prevPrevious) {
            int next = prevPrevious ^ prev.both;

            if (next == NULL) {
                prev.addNext(node);
                node.addPrev(prev);
            } else {
                add(node, referenceToObj.get(next), prev.address);
            }
        }

        Node get(int index) {
            if (index < 0) {
                throw new RuntimeException("Index should be positive!");
            }

            return get(index, 0, NULL, identityHashCode(this.root));
        }

        private Node get(int index, int currIndex, int previousAddr, int currentAddr) {
            if (currentAddr == NULL) {
                return null;
            }

            Node currNode = referenceToObj.get(currentAddr);

            if (index == currIndex) {
                return currNode;
            }

            int nextAddr = previousAddr ^ currNode.both;

            return get(index, currIndex + 1, currentAddr, nextAddr);
        }

    }

}
